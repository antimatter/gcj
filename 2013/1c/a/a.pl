#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use POSIX qw(ceil);
use List::Util qw(min);

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;

$Data::Dumper::Indent = 0;
no warnings 'recursion';

sub debug {
    print STDERR "@_";
    return;
}

## my %CACHE;

use Memoize;
memoize 'findnvalue';
sub findnvalue {
    my ($n, $index, $numarr_ref) = @_;
    my @numarr = @$numarr_ref;
    my $return;
##     debug "findnvalue($n, $index, arr)\n";
    if ($index > @numarr - $n) {
        $return = 0;
    }
    elsif ($numarr[$index] >= $n) {
        $return = @numarr - $index - $n + 1;
    debug "-- Returning $return\n";
    }
    else {
        debug "-- Recursively calling findnvalue($n, $index+1, arr)\n";
        $return = findnvalue($n, $index+1, \@numarr);
    }
##     $CACHE{$n} = $return;
##     debug "-- Returning $return\n";
    return $return;
}

for (my $t = 1; $t <= $T; $t++) {
    my ($name, $n) = split /\s+/, shift @lines;
    my @namearr = split //, $name;
    my @numarr = ();
    my $count = 1;
    for (my $i = @namearr-1; $i >= 0; $i--) {
        if ($namearr[$i] =~ /[aeiou]/) {
            $count = 0;
        }
        $numarr[$i] = $count++;
    }
    my $nvalue = 0;
##     %CACHE = ();
    my $i = 0;
    while ($i < @numarr) {
##     for (my $i = 0; $i < @numarr; $i++) {
##         print "Calling findnvalue($n, $i, arr)\n";
    debug "i=$i, n=$n, num=[@numarr] arr=[@namearr]\n";
    debug "a[i] = $numarr[$i], looking for $n\n";
        my $c = 0;
        if ($numarr[$i] >= $n) {
            my $x = $i + $numarr[$i] - $n +1;
            debug "++ Calling findnvalue($n, $x, arr) = $c\n";
            $c = $numarr[$i] * findnvalue($n, $i+$numarr[$i]-$n+1, \@numarr);
            my $diff = $numarr[$i] - $n +1;
            my $s += ($diff * ($diff + 1)) / 2;
            $c += $s;
            debug "++ c=$c, s=$s\n";
            $i = $i+$numarr[$i];
        }
        else {
            debug ">> Calling findnvalue($n, $i, arr)\n";
            $c = findnvalue($n, $i, \@numarr);
            $i++;
        }
        last if $c == 0;
        $nvalue += $c;
    }

##     printf "Case #$t: $name $n [@namearr] [@numarr] - $nvalue\n";
    printf STDERR "Case #$t: $nvalue\n";
    printf "Case #$t: $nvalue\n";
#debug sprintf "Case #$t: N=$N, A=$A, motes=[@motes] - %d\n\n\n\n\n", calculate(0, $A, @motes);
}


