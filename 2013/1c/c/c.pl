#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use POSIX qw(ceil);
use List::Util qw(min);

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;

## $Data::Dumper::Indent = 0;
no warnings 'recursion';

sub debug {
    return;
    print STDERR "@_";
}

for (my $t = 1; $t <= $T; $t++) {
    my $N = shift @lines;
    my @days_of_attack;
    my %attacks;
    my %wallheight;
    for my $tribe (1 .. $N) {
        my ($d, $n, $w, $e, $s, $d_d, $d_p, $d_s) = split /\s+/, shift @lines;
        for my $attack (1 .. $n) {
            debug "Attack from tribe $tribe - day=$d - strength=$s - range=[$w, $e]\n";
            push @days_of_attack, $d;
            $attacks{$d}{$tribe} = { strength => $s, range => [ $w, $e ] };
            $d += $d_d;
            $s += $d_s;
            $w += $d_p;
            $e += $d_p;
        }
    }
    my $losses = 0;
    foreach my $day (sort { $a <=> $b } keys %attacks) {
        foreach my $tribe (keys %{$attacks{$day}}) {
            no warnings;
            $attacks{$day}{$tribe}{success} = 0;
            my $left = $attacks{$day}{$tribe}{range}[0];
            my $right = $attacks{$day}{$tribe}{range}[1];
            my $strength = $attacks{$day}{$tribe}{strength};
            my $min_height = min(@wallheight{$left .. $right});
            debug "Attack on Day$day - tribe - $tribe ($left, $right) with $strength\n";
            $" = ", ";
            if ($min_height < $strength) {
                $attacks{$day}{$tribe}{success} = 1;
                $losses++;
            }
        }
        foreach my $tribe (keys %{$attacks{$day}}) {
            next if $attacks{$day}{$tribe}{success} == 0;
            my $left = $attacks{$day}{$tribe}{range}[0];
            my $right = $attacks{$day}{$tribe}{range}[1];
            my $strength = $attacks{$day}{$tribe}{strength};
            no warnings;
            $wallheight{$_} = $strength foreach $left .. $right;
            debug "  Loss $losses\n";
        }

    }
    printf "Case #$t: $losses\n";

#debug sprintf "Case #$t: N=$N, A=$A, motes=[@motes] - %d\n\n\n\n\n", calculate(0, $A, @motes);
}


