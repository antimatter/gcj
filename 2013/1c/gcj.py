#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint
from math import log, ceil

DEBUG = 0

def debug (s):
    global DEBUG
    if DEBUG == 1:
        print s

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

@tail_call_optimized
def _factorial (acc, n):
    if (n < 2):
        return acc
    return _factorial(n*acc, n-1)

def factorial (n):
    return _factorial(1, n)

def log2 (n):
    return log(n)/log(2)

@tail_call_optimized
def calculate (turns, my_mote, rest_motes=None):
##     print "calculate(%d, %d, %s)" % (turns, my_mote, rest_motes)
    debug("calculate(%d, %d, [%d])" % (turns, my_mote, len(rest_motes)))
    if len(rest_motes) <= 1:
        if my_mote > rest_motes[0]:
            return turns
        else:
            return turns+1
    if (my_mote > rest_motes[0]):
        return calculate(turns, my_mote+rest_motes[0], rest_motes[1:])
    if (my_mote == 1):
        return len(rest_motes)
    times = 1+int(log2((rest_motes[0]-1)/(my_mote-1)))
##     if times == 0:
##         times = 1
    debug("Calculating min for (turns, my_mote, rest) = (%d, %d, [%d])" % (turns, my_mote, len(rest_motes)))
##     return min(calculate(turns+1, my_mote, rest_motes[:-1]),
##                calculate(turns+times, (2**times)*(my_mote-1)+1, rest_motes))
    return min(calculate(turns+times, (2**times)*(my_mote-1)+1, rest_motes),
               calculate(turns+1, my_mote, rest_motes[:-1]))

    return 0

def main():
    global DEBUG
    if (len(sys.argv) > 2 and sys.argv[2] == "debug"):
        DEBUG = 1

    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))
    testcase = 1
    while testcase <= T:
        (A, N) = map(int, lines.pop(0).split())
        motes = sorted(map(int, lines.pop(0).split()))
        debug("A = %d, motes=%s" % (A, motes))
        print "Case #%d: %d" % (testcase, calculate(0, A, motes))
        testcase += 1
    #print "%d" % calculate(0, 4, [1, 2, 3, 24, 25, 26])
    #print "%d" % calculate(0, 4, [1, 2, 3, 4, 5, 6])
    #print "%d" % calculate(0, 4, [1])
    #print "%d" % calculate(0, 4, [5])
    #print "%d" % calculate(0, 1, [5, 6, 76])
    #print "%d" % calculate(0, 4, [1, 4, 4, 4])
    #print "%d" % calculate(0, 3, [1, 4, 4, 4])

if __name__ == '__main__':
    main()


