#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use POSIX qw(ceil);
use List::Util qw(min);

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;

$Data::Dumper::Indent = 0;
no warnings 'recursion';

sub debug {
    return;
    print STDERR "@_";
}

for (my $t = 1; $t <= $T; $t++) {
    my @input;
    my ($A, $N) = split /\s+/, shift @lines;
    my @motes = split /\s+/, shift @lines;
    @motes = sort { $a <=> $b } @motes;

    printf "Case #$t: \n";
#debug sprintf "Case #$t: N=$N, A=$A, motes=[@motes] - %d\n\n\n\n\n", calculate(0, $A, @motes);
}


