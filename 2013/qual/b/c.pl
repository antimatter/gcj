#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use List::Util ();

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;
my $SIZE = 4;
my ($R, $C);

$Data::Dumper::Indent = 1;

sub debug {
    return;
    print "@_";
}
sub delete_col {
    my ($hashref, $key) = @_;
    debug "Deleting col for $key\n";
    for (my $c = $key % $C; $c < $R * $C; $c+=$C) {
        delete $hashref->{$c};
    }
}
sub delete_row {
    my ($hashref, $key) = @_;
    debug "Deleting row for $key\n";
    for (my $r = int($key/$C)*$C; $r < int($key/$C)*$C+$C; $r++) {
        delete $hashref->{$r};
    }
}
sub printdata {
    my ($hashref) = @_;
    for (my $i = 0; $i < $R; $i++) {
        for (my $j = 0; $j < $C; $j++) {
            if (not defined $hashref->{$i*$C+$j}) {
                debug "x  ";
            }
            else {
                debug $hashref->{$i*$C+$j} . "  ";
            }
        }
        debug "\n";
    }
}
sub is_valid_row {
    my ($hashref, $key) = @_;
    my $v = $hashref->{$key};
    for (my $r = int($key/$C)*$C; $r < int($key/$C)*$C+$C; $r++) {
        next unless defined $hashref->{$r};
        print Dumper $hashref and die if not defined $hashref->{$r};
        if ($hashref->{$r} > $v) {
            return 0;
        }
    }
    return 1;
}
sub is_valid_col {
    my ($hashref, $key) = @_;
    my $v = $hashref->{$key};
    for (my $c = $key % $C; $c < $R * $C; $c+=$C) {
        next unless defined $hashref->{$c};
        if ($hashref->{$c} > $v) {
            return 0;
        }
    }
    return 1;
}

sub check {
    my ($hashref) = @_;
    my @order = List::Util::shuffle(keys %$hashref);
    my @keys = sort { $hashref->{$a} <=> $hashref->{$b} } keys %$hashref;
    return 1 if @keys == 0;
    printdata($hashref);
    debug "keys: [@keys]\n";
    my $lowestkey = $keys[0];
    my $vr = is_valid_row($hashref, $lowestkey);
    my $vc = is_valid_col($hashref, $lowestkey);
    if ($vr and not $vc) {
        delete_row($hashref, $lowestkey);
        return check($hashref);
    }
    elsif (not $vr and $vc) {
        delete_col($hashref, $lowestkey);
        return check($hashref);
    }
    elsif ($vr and $vc) {
        my $hash_r = {%$hashref};
        debug "  => ";
        delete_row($hash_r, $lowestkey);
        my $hash_c = {%$hashref};
        debug "  => ";
        delete_col($hash_c, $lowestkey);
        return check($hash_r) or check($hash_c);
    }
    else {
        debug "Returning 0 (after checkign $lowestkey)\n";
        return 0;
    }
}

for (my $t = 1; $t <= $T; $t++) {
    ($R, $C) = split /\s+/, shift @lines;
    my %data;
    my @data;
    my %serial;
    for (my $r = 0; $r < $R; $r++) {
        my @fields = split /\s+/, shift @lines;
        for (my $c = 0; $c < $C; $c++) {
            $data{"${r}x${c}"} = $fields[$c];
            $data[$r][$c] = $fields[$c];
            $serial{$r*$C+$c} = $fields[$c];
        }
    }

    print "Case #$t: ";
    if (check(\%serial)) {
        print "YES\n";
    }
    else {
        print "NO\n";
    }

}

sub _decide {
    my @entries = @_;
    if (grep { $_ eq "O" } @entries) {
        if ((grep { $_ eq "O" or $_ eq "T" } @entries) == $SIZE) {
            return "O won";
        }
    }
    elsif (grep { $_ eq "X" } @entries) {
        if ((grep { $_ eq "X" or $_ eq "T" } @entries) == $SIZE) {
            return "X won";
        }
    }
    else {
        return "";
    }
}

sub _status_row {
    my $data_ref = shift;
    for (my $i = 0; $i < $SIZE; $i++) {
        my @entries;
        for (my $j = 0; $j < $SIZE; $j++) {
            push @entries, $data_ref->[$i][$j];
        }
        my $result = _decide(@entries);
        return $result if $result =~ /won/;
    }
    return "";
}

sub _status_col {
    my $data_ref = shift;
    for (my $i = 0; $i < $SIZE; $i++) {
        my @entries;
        for (my $j = 0; $j < $SIZE; $j++) {
            push @entries, $data_ref->[$j][$i];
        }
        my $result = _decide(@entries);
        return $result if $result =~ /won/;
    }
}

sub _status_diag {
    my $data_ref = shift;
    my @entries;
    for (my $i = 0; $i < $SIZE; $i++) {
        push @entries, $data_ref->[$i][$i];
    }
    my $result = _decide(@entries);
    return $result if $result =~ /won/;

    @entries = ();
    for (my $i = 0; $i < $SIZE; $i++) {
        for (my $j = 0; $j < $SIZE; $j++) {
            push @entries, $data_ref->[$i][$j] if $i + $j == $SIZE - 1;
        }
    }
    $result = _decide(@entries);
    return $result if $result =~ /won/;
    return "";
}

sub is_draw {
    my $data_ref = shift;
    for (my $i = 0; $i < $SIZE; $i++) {
        for (my $j = 0; $j < $SIZE; $j++) {
            return 0 if $data_ref->[$i][$j] eq ".";
        }
    }
    return 1;
}

sub status {
    my $data_ref = shift;
    my $status_row = _status_row($data_ref);
    return $status_row if $status_row;
    my $status_col = _status_col($data_ref);
    return $status_col if $status_col;
    my $status_diag = _status_diag($data_ref);
    return $status_diag if $status_diag;
    my $status_draw = is_draw($data_ref);
    if ($status_draw) {
        return "Draw";
    }
    else {
        return "Game has not completed";
    }
}

