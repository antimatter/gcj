#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Memoize;

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;
my $SIZE = 4;

$Data::Dumper::Indent = 0;

sub debug {
    print "@_";
    return;
}

sub find_possibilities {
    my ($chest_href, $keys_aref) = @_;
    my @keys_in_hand = @$keys_aref;
    foreach (keys %$chest_href) {
#if $chest_href->{$_}{keytype}
    }
}
sub uniq {
    my @in = @_;
    my @out;
    my %seen;
    foreach (@in) {
        if (not defined $seen{$_}) {
            $seen{$_} = 1;
            push @out, $_;
        }
    }
    return @out;
}
sub use_key {
    my ($keys_ref, $key) = @_;
    for (my $i = 0; $i < @$keys_ref; $i++) {
        my $el = $keys_ref->[$i];
        splice @$keys_ref, $i, 1 and last if $el == $key;
    }
}
sub first_contains_second {
    my ($set, $subset) = @_;
    my @set = @$set;
    my @subset = @$subset;
    foreach my $el (@subset) {
        my $found = 0;
        for (my $i = 0; $i < @set; $i++) {
            if ($el eq $set[$i]) {
                splice @set, $i, 1;
                $found = 1;
                last;
            }
        }
        return 0 unless $found;
    }
    return 1;
}
sub check {

    my ($chest_href, $keys_in_hand_aref, $keytypedb_href, $output_ref) = @_;
    my @totalkeys;
    my @keys = @$keys_in_hand_aref;
    push @totalkeys, @keys;
    push @totalkeys, @{$chest_href->{$_}{keys}} foreach keys %$chest_href;
    my @required_keys = map { $chest_href->{$_}{keytype} } keys %$chest_href;
#    print Dumper $chest_href;
#        print "\n";
#    print Dumper \@totalkeys;
#        print "\n";
    if (@totalkeys < @required_keys) {
        $$output_ref = "IMPOSSIBLE";
        return 0;
    }
    unless (first_contains_second(\@totalkeys, \@required_keys)) {
        $$output_ref = "IMPOSSIBLE";
        return 0;
    }

    debug "\nhere to check with keys in hand: [@keys] ";
    debug "chests: [".join(",", map {"$_=>".$chest_href->{$_}{keytype}} keys %$chest_href)."] ";
    my @possibile_chests;
    foreach my $key (@$keys_in_hand_aref) {
        next unless defined $keytypedb_href->{$key};
        push @possibile_chests, @{$keytypedb_href->{$key}};
    }
    @possibile_chests = uniq(@possibile_chests);
    @possibile_chests = grep { defined $chest_href->{$_} } @possibile_chests;
    @possibile_chests = sort { $a <=> $b } @possibile_chests;
    debug "possib: [@possibile_chests]\n";
    if (keys %$chest_href == 0) {
        return 1;
    }
    if (@$keys_in_hand_aref == 0 or @possibile_chests == 0) {
        debug "Kyes in hand [@$keys_in_hand_aref] 0 or possible chests [@possibile_chests] 0\n";
        $$output_ref = "IMPOSSIBLE";
        return 0;
    }
#    if (@$keys_in_hand_aref < keys %$chest_href) {
#        $$output_ref = "IMPOSSIBLE";
#        return 0;
#    }
    my $wrong = 0;
    foreach my $poss (@possibile_chests) {
        my %chest = %$chest_href;
        my @keys = @$keys_in_hand_aref;
        debug "  checking chest $poss (opens with ";
        debug $chest_href->{$poss}{keytype};
        debug ") which has keys: [";
        debug join ",", @{$chest_href->{$poss}{keys}};
        debug "]\n";
        push @keys, @{$chest_href->{$poss}{keys}};
        use_key(\@keys, $chest_href->{$poss}{keytype});
        delete $chest{$poss};
        my $orig = $$output_ref;
        $$output_ref = "$orig $poss";
        if (check(\%chest, \@keys, $keytypedb_href, $output_ref)) {
            return 1;
        }
        else {
            $$output_ref = $orig;
            $wrong++;
            print "$wrong tries already\n" if $wrong % 100 == 0;
            debug "  opening $poss was not a good idea\n";
        }
    }
}

for (my $t = 1; $t <= $T; $t++) {
    my ($K, $N) = split /\s+/, shift @lines;
    my @keys = split /\s+/, shift @lines;
    my %keytypedb;
    my %chest;

    for (my $i = 1; $i <= $N; $i++) {
        my ($keytype_for_opening, $no_of_keys, @keys_in_chest) = 
            split /\s+/, shift @lines;
        push @{$keytypedb{$keytype_for_opening}}, $i;
        $chest{$i} = {
            keytype => $keytype_for_opening,
            keys => [ @keys_in_chest ],
        };
    }

    foreach (keys %keytypedb) {
        $keytypedb{$_} = [ sort { $a <=> $b } @{$keytypedb{$_}} ];
    }

    my $output = "";
    check(\%chest, \@keys, \%keytypedb, \$output);
    $output =~ s/^\s+//;
    print "Case #$t: $output\n";
}

