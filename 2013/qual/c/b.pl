#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Memoize;

memoize('is_palindrome');

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;
my $SIZE = 4;

$Data::Dumper::Indent = 0;

sub is_palindrome {
    my $in = shift;
    my $o = $in eq reverse $in;
    if ($in eq reverse $in) {
        return 1;
    }
    else {
        return 0;
    }
}

sub is_fair_and_square {
    my $in = shift;
}

use POSIX qw(ceil);

for (my $t = 1; $t <= $T; $t++) {
    my ($A, $B) = split /\s+/, shift @lines;

    my $count = 0;
    for (my $i = ceil(sqrt $A); $i <= int sqrt $B; $i++) {
        if (is_palindrome($i) and is_palindrome($i*$i)) {
            $count++;
        }
    }

    print "Case #$t: $count\n";
}

