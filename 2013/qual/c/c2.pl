#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Memoize;
use threads;

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;
my %CACHE;
my $CACHE_SIZE = 10000000;

$Data::Dumper::Indent = 0;

sub debug {
    return;
    print "@_";
    return;
}

sub is_palindrome {
    my $in = shift;
#return $CACHE{$in} if defined $CACHE{$in};
    my $o = $in eq reverse $in;
    if ($in eq reverse $in) {
        return 1;
    }
    else {
        return 0;
    }
}

sub is_fair_and_square_root {
    my $i = shift;
    #return $CACHE{$i} if defined $CACHE{$i};
    debug "is_fair_square($i)\n";
    my $o = is_palindrome($i) * is_palindrome($i*$i);
    debug sprintf "  Returning $i - %s\n", is_palindrome($i);
    debug sprintf "  Returning $i*$i - %s\n", is_palindrome($i*$i);
    debug sprintf "  Returning $o\n";
    return is_palindrome($i) * is_palindrome($i*$i);
}

use POSIX qw(ceil);
#memoize 'is_palindrome';
#memoize 'is_fair_and_square_root';

sub _fs {
    my ($start, $end) = @_;
    return 0 if $end < $start;

    my $count = 0;
#printf "%d --- %d\n", ceil(sqrt $start), int sqrt $end;
    for (my $i = ceil(sqrt $start); $i <= int sqrt $end; $i++) {
        if (is_fair_and_square_root($i)) {
#print $i*$i . "is a fs\n";
debug $i*$i . "is a fs\n";
            $count++ 
        }
    }

    return $count;
}



sub min {
    my ($a, $b) = @_;
    $a < $b ? $a : $b;
}

sub fs {
    my ($a, $b) = @_;
debug "\nCalled for [$a-$b]\n";
    if ($a % $CACHE_SIZE == 0 and $b == $a + $CACHE_SIZE) {
        if (not defined $CACHE{$a}) {
            $CACHE{$a} = _fs($a, $b);
        }
##         return $CACHE{$a};
        return $CACHE{$a};
    }

    my $start_r = $a;
    my $end_r = min($b,
                    ceil($a / $CACHE2_SIZE)*$CACHE2_SIZE);

    my $count = 0;
#print "S: $start_r, E: $end_r, a: $a, b: $b\n";
    while ($end_r < $b) {
debug "Calling _fs($start_r, $end_r) from inside\n";
        $count += fs($start_r, $end_r);
        $start_r = $end_r;
        $end_r = $start_r + $CACHE2_SIZE;
    }

    my $end_r = min($b,
                    ceil($a / $CACHE2_SIZE)*$CACHE2_SIZE);

    while ($end_r < $b) {
debug "Calling _fs($start_r, $end_r) from inside\n";
        $count += fs($start_r, $end_r);
        $start_r = $end_r;
        $end_r = $start_r + $CACHE_SIZE;
    }
    debug "Finall call _fs($start_r, $b)\n";
    $count += _fs($start_r, $b);
    return $count;
}

for (my $t = 1; $t <= $T; $t++) {
    my ($A, $B) = split /\s+/, shift @lines;

    my $count = 0;
    $count = fs($A, $B);

    print STDERR "Case $t done\n" if $t % 1 == 0;
    print "Case #$t: $count\n";
#print Dumper \%CACHE;
}

