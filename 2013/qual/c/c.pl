#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Memoize;
use threads;

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;
my $SIZE = 4;
my %CACHE;

$Data::Dumper::Indent = 0;

sub is_palindrome {
    my $in = shift;
#return $CACHE{$in} if defined $CACHE{$in};
    my $o = $in eq reverse $in;
    if ($in eq reverse $in) {
        return 1;
    }
    else {
        return 0;
    }
}

sub is_fair_and_square_root {
    my $i = shift;
    #return $CACHE{$i} if defined $CACHE{$i};
    return is_palindrome($i) and is_palindrome($i*$i);
}

use POSIX qw(ceil);
#memoize 'is_palindrome';
#memoize 'is_fair_and_square_root';

for (my $t = 1; $t <= $T; $t++) {
    my ($A, $B) = split /\s+/, shift @lines;

    my $count = 0;
    for (my $i = ceil(sqrt $A); $i <= int sqrt $B; $i++) {
        #my $thread1 = threads->create('is_fair_and_square')
        #if ($i % 2) {
        #}

        if (is_fair_and_square_root($i)) {
            $CACHE{$i} = 1;
            $count++ 
        }
        else {
            $CACHE{$i} = 0;
        }
        #if (is_palindrome($i) and is_palindrome($i*$i)) {
        #    $count++;
        #}
    }

    print STDERR "Case $t done\n" if $t % 100 == 0;
    print "Case #$t: $count\n";
}

