#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use POSIX qw(ceil);
use List::Util qw(min);

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;

$Data::Dumper::Indent = 0;
no warnings 'recursion';

sub debug {
    return;
    print STDERR "@_";
}

for (my $t = 1; $t <= $T; $t++) {
    my @input;
    my ($A, $N) = split /\s+/, shift @lines;
    my @motes = split /\s+/, shift @lines;
    @motes = sort { $a <=> $b } @motes;

    printf "Case #$t: %d\n", calculate(0, $A, @motes);
#debug sprintf "Case #$t: N=$N, A=$A, motes=[@motes] - %d\n\n\n\n\n", calculate(0, $A, @motes);
}


sub calculate {
    my ($n, $a, @motes) = @_;
    debug "Started with: ($n, $a, [@motes])\n";
    my $i = 0;
#while (@motes > 1 and ($a + $motes[0] < $motes[1] or $a > $motes[0])) {
    while (@motes > 1 and $a > $motes[0]) {
        $a += shift @motes;
    }
    debug "Reduced to: ($n, $a, [@motes])\n";
    if (@motes == 1) {
        if ($a > $motes[0]) {
        debug "Returning $n\n";
            return $n;
        }
        else {
        debug "Returning $n+1\n";
            return $n+1;
        }
    }
    else {
        if ($a == 1) {
        debug "Returning (\@motes)=" . scalar @motes. "\n";
            return scalar @motes;
        }
        else {
            my $times;
            $times = ceil(log2(($motes[0]-1)/($a-1)));

            my @new_motes = @motes;
            pop @new_motes;

            my $delete_calculation = calculate($n+1, $a, @new_motes);
#            if ($times == 0) {
#                debug "Deleting as $times = 0 ($n, $a, [@motes])\n";
#                return $delete_calculation;
#            }
#            else {
    $times = 1 if $times == 0;
                my $new_a = $a * (2**$times) - (2**$times) + 1;
                my $add_calculation = calculate($n+$times, $new_a, @motes);
                debug "Will calculate($n+$times, $new_a, [@motes]) and calculate($n+1, $a, [@new_motes])\n";
                return min($add_calculation, $delete_calculation);
#            }
        }
    }
}

sub log2 {
    return log(shift)/log(2);
}
