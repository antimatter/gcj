#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);

my @lines = map { chomp; $_ } <>;
my $T = shift @lines;

$Data::Dumper::Indent = 0;

for (my $t = 1; $t <= $T; $t++) {
    my ($E, $R, $N) = split /\s+/, shift @lines;
    my @v = split /\s+/, shift @lines;

    print "Case #$t: $E-$R-$N-[@v]\n";
}


sub debug {
    print "$@";
    return;
}
