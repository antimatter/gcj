#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

## @tail_call_optimized


def mkdir(filesystem, path):
    for p in path.split('/'):
        print p
##         if p == path:
##             return
    return
    for p in path.split('/'):
        base = base + '/' + p
        filesystem.append(base)
    filesystem.sort()
    return

#f = ['/a', '/b', '/aa']
#mkdir (f, '/aa/b/c/c/c')
#mkdir (f, '/aa/b/x')
#sys.exit(0)

def maketree(dir):
    tree = {}
    print sorted(dirs)
    for dir in sorted(dirs):
        if not intree(parent(dir)):
            maketree(parent(dir))
        tree[parent(dir)] 
        components = dir.split("/")
        for component in components:
            if not component in 

def main():
    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))
    testcase = 0
    while testcase < T:
        tree = {}
        testcase += 1
        N, M = map(int, lines.pop(0).split())
##         print "N = %d, M = %s" % (N, M)
        present = [ lines.pop(0).rstrip('\n') for i in xrange(N) ]
        tomake = [ lines.pop(0).rstrip('\n') for i in xrange(M) ]
        tree = maketree(present)
        print "Case #%d: " % (testcase)
#        present.sort()
#        final.sort()
#mkdir(present, sorted(final))
#print sorted(final)
##         print "%s - %s" % (present, final)
##         print "-----"

if __name__ == '__main__':
    main()
