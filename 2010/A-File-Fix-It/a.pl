#! /usr/bin/perl -w

use strict;
use List::Util qw(max first sum);
use Data::Dumper qw(Dumper);

sub debug {
    local $" = "\n";
    print "@_";
}

sub _localtree {
    my ($comp_ref, $index) = @_;
    if ($index >= @{$comp_ref}) {
        return {};
    }
    else {
        return { $comp_ref->[$index] => _localtree($comp_ref, $index+1) };
    }
}

sub buildtree {
    my ($treeref, $entry) = @_;
    my @components = split "/", $entry;
    shift @components;
    my $path = $treeref->{"/"};
    for (my $i = 0; $i < @components; $i++) {
        if (not defined $path->{$components[$i]}) {
            $path->{$components[$i]} = _localtree(\@components, $i+1);
            return unless defined wantarray;
##             printf "Returning - %d\n", @components - $i;
            return @components - $i;
        }
        $path = $path->{$components[$i]};
    }
}

sub buildtree_count {
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    for my $t (1 .. $T) {
        my $count = 0;
        my %tree = ( "/" => {} );
        my ($N, $M) = split /\s+/, shift @lines;
        buildtree(\%tree, shift @lines) for 1 .. $N;
##         print "Wanted:\n";
##         shift @lines for 1 .. $M;
        $count += buildtree(\%tree, shift @lines) for 1 .. $M;
        print "Case #$t: $count\n";

##         print Dumper(\%data);
##     print Dumper \%stats;
    }
}


main();

