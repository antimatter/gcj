#! /usr/bin/perl -w

use strict;

sub run {
    my ($index, $k, @queue) = @_;
    local $" = ", ";
##     print "Here with -> index: $index, k: $k, [@queue]\n";

##     my $groups = scalar @queue;
##     print "groups = $groups\n";

    my $people = 0;
    my $shift;
    for my $i (map { ($index + $_) % $#queue } 0 .. $#queue-1) {
        if ($people + $queue[$i] > $k) {
            $shift = $i;
            last;
        }
        $people += $queue[$i];
    }
    return $people, $shift;
}

sub main {
    my @lines = <>;
    my $T = shift @lines;
    for my $t (1 .. $T) {
        my ($R, $k, $N) = split /\s/, shift @lines;
        my @queue = split /\s/, shift @lines;

        my $index = 0;
        my $euros = 0;
        my $people = 0;
        for (0 .. $R) {
##             print "Running with $index, $k, [@queue] <N: $R>\n";
            ($people, $index) = run($index, $k, @queue);
            $euros += $people;
        }
        print "Case #$t: $euros\n";
    }
##     ($people, $index) = run(2, 8, 2, 3, 4, 5, 1, 3, 2);
##     ($people, $index) = run($index, 8, 2, 3, 4, 5, 1, 3, 2);
}

main();
## run(0, 5, 1, 2, 3, 4, 2, 1, 3, 1, 3, 4, 3);
