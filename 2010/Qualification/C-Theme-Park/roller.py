#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

def select(queue, k, index=0):
    groups = len(queue)
    people = 0
    shift = 0
    for i in xrange(index, index+groups):
        i = i % groups
        if people + queue[i] > k:
            break
        people += queue[i]
##     print "%s(%d) -> %d (%d)" % (queue, index, people, i)
    return people, i

store = []
values = {}

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

@tail_call_optimized
def runall(queue, k, index, n, euros):
    global store, values
##     print "Here with: %d, %d, %d, %d" % (k, index, n, euros)
    if n == 0:
        return euros
    if index in store:
        where = store.index(index)
        repeat_count = len(store[where:])
        repeat_sum = sum(map(lambda x: values[x],
                             store[where:]))
        partial_sum = sum(map(lambda x: values[x],
                              store[where:where+(n%repeat_count)]))
##         print "%d: Found %d at %d (n=%d, sum=%d, cnt=%d, psum=%d" % (
##                     repeat_sum*(n/repeat_count), index, where,
##                     n, repeat_sum, repeat_count, partial_sum)
        return euros + (repeat_sum*(n/repeat_count)) + partial_sum
##         return runall(queue, k, index, n%repeat_count, euros+(repeat_sum*(n/repeat_count)))
##         return 
    people, newindex = select(queue, k, index)
    store.append(index)
    values[index] = people
    euros += people
    return runall(queue, k, newindex, n-1, euros)

def main():
    global store, values
    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))
    testcase = 0
    while testcase < T:
        testcase += 1
        R, k, N = map(int, lines.pop(0).split())
        queue = map(int, lines.pop(0).split())
##         print "R: %d, k: %d, N: %d, G: %s" % (R, k, N, queue)

##         index = 0
##         euros = 0
        store = []
        values = {}
        euros = runall(queue, k, 0, R, 0)
        print "Case #%d: %d" % (testcase, euros)
##     select([2, 4, 2, 3, 4, 2, 1, 2, 1, 3], 11, 9)

if __name__ == '__main__':
    main()
