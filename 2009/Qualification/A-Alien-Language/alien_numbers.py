#! /usr/bin/python
import sys
import pprint
import re

pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

## @tail_call_optimized
def combinations(result, args, dictionary):
    i = 0
    string = ""
    while len(args[i]) == 1:
        string += args[i][0]
        if not any(map(lambda w: w.startswith(string), dictionary)):
            return
        i += 1
        if i == len(args):
            result.append(string)
            return
    this_arg = args[i][:]
    argscopy = args[:]
    for letter in this_arg:
        argscopy[i] = letter
        combinations(result, argscopy, dictionary)
    return

def main():
##     re.sub(r"[)(]+", " ", re.sub(r"^\(|\)$", "", test))
    f = open(sys.argv[1])
    lines = f.readlines()
    L, D, N = map(int, lines.pop(0).split(' '))
    dictionary = [ line.replace('\n', '') for line in lines[0:D] ]
    words = [ list(eval(re.sub(r'\(([a-z]+)\)',
                 lambda x: str(list(x.groups()[0])) + ', ',
                 re.sub(r'(^|\))([a-z]+)(\(|$)',
                        r"\1['\2'], \3", line.replace('\n', '')))))
             for line in lines[D:] ]

##     print "Dictionary: %s" % dictionary
    i = 0
    for i in xrange(len(words)):
##         print words[i]
        result = []
        combinations(result, words[i], dictionary)
        print "Case #%d: %d" % (i+1, len(result))
        continue
##                  lambda x: str(list(x.groups()[0],
##     pp(words)

if __name__ == '__main__':
    main()
