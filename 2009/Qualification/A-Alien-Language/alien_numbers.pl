#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);

my @lines = map { chomp; $_ } <>;

my ($L, $D, $N) = split /\s/, shift @lines;
my %dictionary = map { $_ => undef } @lines[0 .. $D];
my @testcases = map { chomp; $_ } @lines[$D+1 .. $#lines];

undef @lines;
print Dumper(\@testcases);

## print Dumper(\%dictionary);
print "\n";

