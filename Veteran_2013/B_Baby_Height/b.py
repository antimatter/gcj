#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

## @tail_call_optimized

def to_inches(string):
    import re
    out = []
    try:
        out = re.match('(\d+)\'(\d+)"', string).groups()
    except AttributeError:
        print "Invalid Height"
        return
    return (int(out[0]) * 12) + int(out[1])

def from_inches(inches):
#print "Heree with %d" % inches
    feet = inches / 12
    inch = inches % 12
#    print "%d => %d'%d\"" % (inches, feet, inch)
    return "%d'%d\"" % (feet, inch)

def main():
    from math import ceil, floor
    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))
    testcase = 1
    while testcase <= T:
        sex, mother, father = lines.pop(0).rstrip().split(" ")
        height = to_inches(mother) + to_inches(father)
        if (sex == "G"):
            height -= 5
        elif (sex == "B"):
            height += 5
        height /= 2.0
#print "%f" % height
        lower = height - 4
        upper = height + 4
        lower = ceil(height - 4)
        upper = floor(height + 4)

        print "Case #%d: %s to %s" % (testcase,
                                      from_inches(lower),
                                      from_inches(upper))
        testcase += 1

if __name__ == '__main__':
    main()
