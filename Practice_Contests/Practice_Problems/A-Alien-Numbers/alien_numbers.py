#! /usr/bin/python

import sys

file = open(sys.argv[1])
count = int(file.readline())

def decimal_to_base(number, base):
    retval = []
    while number:
        remainder = number % base
        retval.insert(0, remainder)
        number = (number - remainder)/base
    return retval

def change_base(numarray, indict, outdict):
    inbase = len(indict)
    outbase = len(outdict)
    numlen = len(numarray)
    number = sum(indict[x] * inbase ** i
                 for i, x in enumerate(reversed(numarray)))
    outnumarray = decimal_to_base(number, outbase)
    return map(lambda x: outdict[x], outnumarray)

def test():
    ind = {'D': 0, '>': 1, 'a': 1, 's': 2, 'C': 3, 'O': 4}
    outd = {0: 'a', 1: ',', 2: '$', 3: 'X', 4: '}', 5: '(', 6: 'F',
            7: '3', 8: '3', 9: 'z', 10: '9'}
    print change_base('OD', ind, outd)

for i in xrange(0, count):
    number, char_source, char_dest = file.readline().split()
    indict = dict((x, i) for i, x in enumerate(char_source))
    outdict = dict((i, x) for i, x in enumerate(char_dest))
    print "Case #%d: %s" % (i+1, "".join(change_base(number,
                                                     indict,
                                                     outdict)))
