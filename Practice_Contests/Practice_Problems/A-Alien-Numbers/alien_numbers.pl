#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);

open(F, "<", "large.in") or die "Cannot open input file\n";

sub from_base($$) {
    my ($representation, $string) = @_;
    my $base = length $representation;

    my @symbols = split //, $representation;
    my %weights = ();
    foreach my $i (0 .. $#symbols) {
        $weights{$symbols[$i]} = $i;
    }

    my @input = reverse split //, $string;
    my $retval = 0;
    $retval += $weights{$input[$_]} * ($base ** $_) for 0 .. $#input;
    return $retval;
}

## print from_base("abE", "bEEaab");
## print from_base("0123456789abcdef", "23a3");

sub to_base($$) {
    my ($representation, $number) = @_;
    my $base = length $representation;
    my @symbols = split //, $representation;
    my $converted = "";
    while ($number) {
        my $remainder = $number % $base;
        $converted = $symbols[$remainder] . $converted;
        $number = ($number - $remainder) / $base;
    }
    return $converted;
}

## print to_base('0123456789abcdef', 3039);

my $count = <F>;
for (my $i = 0; $i < $count; $i++) {
    $_ = <F>;
    chomp;
    my ($number, $source_lang, $dest_lang) = split;
    printf "Case #%d: %s\n",
           $i + 1,
           to_base($dest_lang, from_base($source_lang, $number));
}

