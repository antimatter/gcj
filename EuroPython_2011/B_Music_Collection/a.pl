#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);

my @lines = map { chomp; $_ } <>;

my $T = shift @lines;

sub substrings_of_length {
    my ($string, $length) = @_;
    my @substrings;
    for (my $i = 0; $i <= length($string) - $length; $i++) {
        push @substrings, substr $string, $i, $length;
    }
    return @substrings;
}

sub is_impossible {
    my ($impossible_ref, $string) = @_;
    if (length $string == 1) {
        print "Was here with [$string]\n";
    }
    return 0 if length $string == 1;
    return defined $impossible_ref->{lc substr $string, 0, length($string)-1};
}

sub search {
    my ($array_ref, $impossible_ref, $search_string) = @_;
    return 0 if defined $impossible_ref->{$search_string};
    return grep { /$search_string/i } @$array_ref;
}

sub uniq {
    my @array = @_;
    my %seen;
    return map { $_->[0] } grep { $_->[1] == 1 } map { [ $_, ++$seen{$_} ] } @array;
}

for my $testcase (1 .. $T) {
    my $no_of_tracks = shift @lines;
    my @tracks;
    push @tracks, shift @lines for 1 .. $no_of_tracks;
    print "Case #$testcase:\n";
    print "\"\"\n" and next if @tracks == 1;
#print Dumper \@tracks if $testcase == 71 or $testcase == 72;
    my %matches;
    my %impossible;
    foreach my $track (sort { length($a) <=> length($b) } @tracks) {
        my $found = 0;
        for (my $length = 1; $length <= length $track; $length++) {
            my @substrs = uniq substrings_of_length($track, $length);
            foreach my $substring (sort @substrs) {
#print "Analyzing [$substring]\n";
                my $search_results = search(\@tracks, \%impossible, $substring);
                if ($search_results == 1) {
                    push @{$matches{$track}}, $substring;
                    $found = 1;
                }
                else {
                    $impossible{$substring} = 1;
#print "[$substring] has many possible matches\n";
                }
            }
            last if $found;
            #foreach my $substr (substrings_of_length($track, $length)) {
            #    print "  $track - $length - $substr\n";
            #}
        }
    }
    foreach my $track (@tracks) {
        my $result = defined $matches{$track} ? sprintf "\"%s\"", uc((sort @{$matches{$track}})[0]) : ":(";
        print "$result\n";
    }
}

#my ($L, $D, $N) = split /\s/, shift @lines;
#my %dictionary = map { $_ => undef } @lines[0 .. $D];
#my @testcases = map { chomp; $_ } @lines[$D+1 .. $#lines];
#
#undef @lines;
#print Dumper(\@testcases);

## print Dumper(\%dictionary);

