#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

## @tail_call_optimized

VOWELS = ("a", "e", "i", "o", "u")

def is_vowel(character):
    for vowel in VOWELS:
        if character == vowel:
            return True
    return False

def main():
    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))
    testcase = 1
    while testcase <= T:
        country = lines.pop(0).rstrip()
        lastchar = country[-1].lower()
        result = "Case #%d: %s is ruled by " % (testcase, country)
        if (lastchar == "y"):
            result = result + "nobody."
        elif (is_vowel(lastchar)):
            result = result + "a queen."
        else:
            result = result + "a king."
        print result
        testcase += 1

if __name__ == '__main__':
    main()
