from sys import argv

def open_file(file_name):
    fin = open(file_name)
    lines = fin.readlines()
    fin.close()
    test_cases = lines[0] # who needs this crap
    del lines[0]
    # CN 3C0 ... 3CN DN 2D0 .... 2DN n 0 ... n
    # C's are combined words
    # D's are opp words
    # N are the list to be invoked
    for line in lines:
        line = line.strip()
        print "line is ", line
        combo_dict = {}
        opp_dict = {}
        prb_list = []
        kase = line.split()
        # get no. of combowords
        no_of_combo = int(kase[0])
        del kase[0]
        combo_dict = get_combo_dict(no_of_combo, kase)
        print "combo is ", combo_dict
        kase = clear_list(kase, no_of_combo)
        no_of_opp = int(kase[0])
        del kase[0]
        opp_dict = get_opp_dict(kase,no_of_opp)
        kase = clear_list(kase, no_of_opp)
        prb_list = kase[1]
        print "opp is ", opp_dict
        print "prb is ", prb_list


def get_combo_dict(limit, line):
    cmd_dict = {}
    for i in range(limit):
        wrd = line[i]
        cmd_dict[wrd[:2]] = wrd[-1]
    return cmd_dict

def get_opp_dict(t, limit):
    opp_dic = {}
    for i in range(limit):
        wrd = t[i]
        opp_dic[wrd] = ''
    return opp_dic
    
def clear_list(list, limit):
    for i in range(limit):
        list.pop(0)
    return list

script, file_name = argv
open_file(file_name)
