import os
import sys
import math
def form_chain(nonplace):
	#print nonplace
	npcounter = 0;
	chain_list = []
	while(npcounter<len(nonplace)):
		#print "+++++"*2;
		current_chain = [];
		if(nonplace[npcounter] !=0):
			
			current_chain.append(npcounter);
			want_pos = nonplace[npcounter]-1;
			#print "Iam pos %d i want pos %d" %(npcounter, want_pos)
			while True:
				if want_pos in current_chain:
					break;
				else:					
					temp = nonplace[want_pos]-1;
					current_chain.append(want_pos)
					nonplace[want_pos] = 0;
					#print "Iam pos %d i want pos %d" %(want_pos, temp)
					want_pos = temp;
			#print current_chain
			#print "++++++"*2
			chain_list.append(len(current_chain))		
		npcounter = npcounter+1;
	print chain_list

def hitcount(sort_list, number_count):

	#print sort_list
	out_of_place =0;
	nonplace=[0]*number_count
	for counter in range(0, number_count):
		if(counter+1!=int(sort_list[counter])):
			nonplace[counter] = int(sort_list[counter])
		#print counter , sort_list[counter], out_of_place
	#print out_of_place
	
	form_chain(nonplace);
	return (out_of_place,out_of_place+1)[out_of_place%2 !=0]
	
	
if __name__ == "__main__":
	in_handle =  open('D-small-attempt2.in', "r");
	test_case_count = int(in_handle.readline());
	test_case_counter = 1;
	while(test_case_counter<test_case_count+1):
		number_count = int(in_handle.readline())
		sort_list = in_handle.readline().split()
		print "Case #%d: %.6f" %(test_case_counter, hitcount(sort_list, number_count))
		print "-------------------------------------------"*2
		test_case_counter = test_case_counter + 1