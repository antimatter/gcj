#! /usr/bin/perl -w

use strict;
use List::Util qw(shuffle);

my $N = 100000;
## my $N = 5;

my @a = (1, 2, 3, 4);
my @sorted = sort @a;

sub same ($$) {
    my ($first, $second) = @_;
    return 0 unless @$first == @$second;
    for (my $i = 0; $i < @$first; $i++) {
        return 0 if $first->[$i] ne $second->[$i];
    }
    return 1;
} 

for (my $run = 0; $run < $N; $run++) {
INNER:
    for (my $i = 1; ; $i++) {
        my @tmp = shuffle @a;
##         my @a = @tmp;
##         print "i = $i [@tmp] [@sorted]\n";
        if (same(\@tmp, \@sorted)) {
            print "$i\n";
            last INNER;
        }
    }
}

