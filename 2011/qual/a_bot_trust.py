#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

## @tail_call_optimized

def main():
    f = open(sys.argv[1])
    lines = map(str.rstrip, f.readlines())
    T = int(lines.pop(0))
    testcase = 1
    while testcase <= T:
        print "Case #%d:" % (testcase),

        totaldata = []
        o_data = []
        b_data = []
        moveto = []
        state = {
            'O': {
                'pos':  1,
                'moveto':  [],
                'press?': 0,
            },
            'B': {
                'pos':  1,
                'moveto':  [],
                'press?': 0,
            },
        }

        totaldata = lines.pop(0).split(' ')
        N = int(totaldata.pop(0))
        i = 0
        print "N = ", N
        while i < 2*N:
##             print "checking %d - %s - %s" % (i, totaldata[i], totaldata[i+1])
            bot = totaldata[i]
            pos = int(totaldata[i+1])
            state[bot]['moveto'].append(pos)
            moveto.append((bot, pos))
            i = i+2

        time = 0
        nextbotpress = moveto.pop(0)[0]
        def oneposition():
            if state[bot]['moveto'][0] > state[bot]['pos']:
                return 1
            elif state[bot]['moveto'][0] < state[bot]['pos']:
                return -1
            return 0

        time = 0
        while True:
            time += 1
            print "time: %d ----------------" % time;
            if len(moveto) == 0:
                break
            for bot in ['O', 'B']:
                if state[bot]['press?'] == 1 and nextbotpress == bot:
                    print "%s pressing button at %d" % (bot, state[bot]['pos'])
                    state[bot]['moveto'].pop(0)
                    nextbotpress = moveto.pop(0)[0]
                elif state[bot]['press?'] == 1:
                    print "%s staying at %d" % (bot, state[bot]['pos'])
                else:
                    newpos = state[bot]['pos'] + oneposition()
                    print "%s moving from %d to %d towards %d" % (
                        bot, state[bot]['pos'], newpos, state[bot]['moveto'][0])
                    state[bot]['pos'] = newpos
                    if state[bot]['moveto'][0] == newpos:
                        state[bot]['press?'] = 1

        print time
        print state
        print moveto
        testcase += 1

if __name__ == '__main__':
    main()
