#! /usr/bin/perl -w

use strict;
use List::Util qw(reduce min sum);


sub main {
    my @lines = <>;
    my $T = shift @lines;
    for my $t (1 .. $T) {
        chomp (my $N = shift @lines);
        my @c = map(int, split /\s/, shift @lines);
        if (reduce { $a ^ $b } @c) {
            print "Case #$t: NO\n";
        }
        else {
            printf "Case #$t: %d\n", (sum @c) - (min @c);
        }
    }
}

main();

