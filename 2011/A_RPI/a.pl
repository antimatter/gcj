#! /usr/bin/perl -w

use strict;
use List::Util qw(max first sum);
use Data::Dumper qw(Dumper);

sub debug {
    return;
    local $" = "\n";
    print "@_";
}

sub calc_owp {
    my ($stats, $team, $data) = @_;
    my %stats = %{$stats};
    print "----$team-----\n";
    my %d = %{$stats{$team}{OWP_data}};
    print Dumper \%d;
    print "---..------\n";
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    @lines = map { chomp; $_ } @lines;
    my @data = ();
    for my $t (1 .. $T) {
        my $N = shift @lines;
        print "Case #$t:\n";
        foreach my $i (0 .. $N-1) {
            @{$data[$i]} = split //, shift @lines;
        }
        my %stats;
        foreach my $team (0 .. $N-1) {
            $stats{$team}{games} = 0;
            $stats{$team}{totalwins} = 0;
            foreach my $opponent (0 .. $N-1) {
                unless ($data[$team][$opponent] eq ".") {
                    $stats{$team}{games}++;
                    $stats{$team}{totalwins}++ if $data[$team][$opponent] eq "1";
                }
            }
            $stats{$team}{WP} = $stats{$team}{totalwins} / $stats{$team}{games};
            my $owp = 0;
            my $n = 0;
            foreach my $opponent (0 .. $N-1) {
                if ($data[$team][$opponent] eq ".") {
##                     $stats{$team}{OWP_data}{$opponent} = $stats{$team}{WP};
                }
                else {
                    $stats{$opponent}{OWP_data}{$team} = ($stats{$team}{totalwins} - $data[$team][$opponent]) / ($stats{$team}{games} - 1);
                    $owp += $stats{$opponent}{OWP_data}{$team};
##                     print "Writing stats for $opponent from $team = $stats{$opponent}{OWP_data}{$team}\n";
                    $n++;
                }
            }
        }
        foreach my $team (0 .. $N-1) {
            my @a = values %{ $stats{$team}{OWP_data} };
            $stats{$team}{OWP} = sum(@a) / scalar @a;
        }
        foreach my $team (0 .. $N-1) {
            my @oowp = map { $stats{$_}{OWP} } keys %{ $stats{$team}{OWP_data} };
            $stats{$team}{OOWP} = sum(@oowp) / @oowp;

            $stats{$team}{RPI} = 0.25 * $stats{$team}{WP} + 0.50 * $stats{$team}{OWP} + 0.25 * $stats{$team}{OOWP};
            print "$stats{$team}{RPI}\n";
        }

##         print Dumper(\%data);
##     print Dumper \%stats;
    }
}


main();

