#! /usr/bin/perl -w

use strict;
use List::Util qw(max first sum);
use Data::Dumper qw(Dumper);

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    my %walls = ();
    for my $t (1 .. $T) {
        my ($N, $M) = split /\s+/, shift @lines;
        print "Case #$t:\n";
        my @start = split /\s+/, shift @lines;
        my @finish = split /\s+/, shift @lines;
        foreach my $i (0 .. $M-1) {
            push @{$walls{$start[$i]}}, $finish[$i];
            push @{$walls{$finish[$i]}}, $start[$i];
        }

        my $state = undef;
        foreach my $w (1 .. $N) {
            next if not defined $walls{$w};
            $state = ";
        }
    print Dumper \%walls;
    }
}


main();

