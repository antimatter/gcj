#! /usr/bin/perl -w

use strict;
use List::Util qw(max first);
use Data::Dumper qw(Dumper);

my $mystery;
my @mystery = ();
my $blackboard;
my @blackboard = ();
my $points = 0;
my $debug = 1;

sub debug2 {
    local $" = "\n";
    print "@_" if $debug >= 2;
}

sub debug1 {
    local $" = "\n";
    print "@_" if $debug >= 1;
}

sub set_mystery {
    my ($word) = @_;
    $mystery = $word;
    @mystery = split //, $mystery;
    $blackboard = "_" x @mystery;
    @blackboard = split //, $blackboard;
    $points = 0;
    debug1 "Setting mystery word: $mystery $blackboard\n";
}

sub guess {
    my ($letter) = @_;

    my @pos = ();
    debug2 "Guessing $letter on $mystery ($blackboard) - ";
    for (my $i = 0; $i < @mystery; $i++) {
        if ($mystery[$i] eq $letter) {
            push @pos, $letter;
            $blackboard[$i] = $letter;
        }
    }
    if (@pos) {
        $blackboard = join "", @blackboard;
        debug2 "RIGHT (points = $points) $blackboard\n";
    } else {
        $points += 1;
        debug2 "WRONG (points = $points)\n";
    }
    return @pos;
}

sub letter_in_words {
    my ($letter, @words) = @_;
    foreach my $word (@words) {
        if (index($word, $letter) != -1) {
            return 1;
        }
    }
    return 0;
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
TESTCASE:
    for my $t (1 .. $T) {
        my (@D, @L);
        my ($N, $M) = split /\s+/, shift @lines;
        my %points = ();
        my %max = ();
        for (my $i = 0; $i < $N; $i++) {
            push @D, shift @lines;
        }
        for (my $i = 0; $i < $M; $i++) {
            push @L, shift @lines;
        }

LETTERSEQ:
        foreach my $letterseq (@L) {
MYSTERYWORD:
            foreach my $word (@D) {
                my @letters = split //, $letterseq;
                debug2 "letters: [@letters]\n";
                debug1 "For seq: $letterseq ";
                set_mystery $word;
                my @poss_words = grep { length $_ == length $blackboard } @D;
                debug1 "Eliminated words of wrong length - [@poss_words] [@D]\n";
                until (@poss_words == 1 or @letters == 0) {
                    my $letter = shift @letters;
##                 foreach my $letter (@letters) {
                    if (letter_in_words $letter, @poss_words) {
                        debug1 "Taking guess: $letter, blackboard: $blackboard, mystery: $mystery [@poss_words]\n";
                        my @pos = guess $letter;
                        if (@pos) {
                            @poss_words = grep { index($_, $letter) != -1 } @poss_words;
                            debug1 "YES!! Removed words that didnt have $letter - [@poss_words]\n";
                        }
                    }
                }
                $points{$letterseq}{$word} = $points;
                my $max = max values %{$points{$letterseq}};
                $max{$letterseq} = first { $points{$letterseq}{$_} == $max } @D;
                debug1 "EXITING: < $max{$letterseq} > $points [@poss_words] [@letters]\n\n\n\n\n";
            }
        }
        debug1 Dumper \%points;
        printf "Case #$t: %s \n", join " ", map { $max{$_} } @L;
    }
}

main();

