#! /usr/bin/perl -w

use strict;
use List::Util qw(max first);
use Data::Dumper qw(Dumper);

sub debug {
    return;
    local $" = "\n";
    print "@_";
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    for my $t (1 .. $T) {
        my (@D, @L);
        my ($N, $M) = split /\s+/, shift @lines;
        for (my $i = 0; $i < $N; $i++) {
            push @D, shift @lines;
        }
        for (my $i = 0; $i < $M; $i++) {
            push @L, shift @lines;
        }

        my @answers;
        my %data;
        foreach my $letterseq (@L) {
            debug "[@D] $letterseq =============\n";
            my %points = ();
            foreach my $word (@D) {
                my $board = "_" x length $word;
                my @poss_words = grep { length $_ == length $word } @D;
                $points{$word} = points($word, $board, $letterseq, @poss_words);
                $data{$letterseq}{$word} = $points{$word};
                debug "$word -> $points{$word}\n";
            }
            debug "\n\n\n";
            my $max = max values %points;
            push @answers, first { $points{$_} == $max } @D;
        }
        print "Case #$t: @answers\n";
##         print Dumper(\%data);
    }
}

sub guessletter {
    my ($word, $board, $letter) = @_;
##     print "GUESSING $letter in $board for $word\n";
    my @wordletters = split //, $word;
    my @boardletters = split //, $board;
    my @positions = ();
    for (my $i = 0; $i < scalar @wordletters; $i++) {
        if ($wordletters[$i] eq $letter) {
            $boardletters[$i] = $letter;
            push @positions, $i;
        }
    }
    return join("", @boardletters), @positions;
}

sub points {
    my ($word, $board, $letterseq, @allwords) = @_;
##     print "--------------------- $word ---------------------\n";
    my $points = 0;
    if ($board eq $word or @allwords == 1 or length $letterseq == 0) {
        return $points
    }
    my @letters = split //, $letterseq;
    my (@words, $letter);
    until ($board eq $word or @allwords == 1 or @letters == 0) {
        $letter = shift @letters;
        @words = grep { index($_, $letter) != -1 } @allwords;
        if (@words) {
##             print "WOULD HAVE GUESSED for $letter in [@allwords] [@words]\n";
            my ($b, @pos) = guessletter $word, $board, $letter;
            if (@pos) {
                $board = $b;
                @allwords = grep { my @w = split //, $_;
                                   my @c = split //, $word;
                                   foreach my $i (@pos) {
                                       if ($w[$i] ne $c[$i]) {
                                           0;
                                       }
                                   }
                                   1;
                                 } @words;
##                 print "Got $letter - $word, $board [@allwords] = $points\n";
            } else {
                @allwords = grep { index($_, $letter) == -1 } @allwords;
                $points += 1;
            }
        }
    }
##     print "<$word $board $letterseq [@words] [@allwords] === $points>\n";
    return $points;

}

main();

