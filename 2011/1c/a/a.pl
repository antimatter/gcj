#! /usr/bin/perl -w

use strict;
use List::Util qw(max first sum);
use Data::Dumper qw(Dumper);

sub debug {
    local $" = "\n";
    print "@_";
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    my @data = ();
    my @painting = ();
TESTCASE:
    for my $t (1 .. $T) {
        my ($R, $C) = split /\s+/, shift @lines;
        print "Case #$t:\n";
        foreach my $r (0 .. $R-1) {
            @{$painting[$r]} = split //, shift @lines;
        }

        local $^W = 0;
##         painting(\@painting, $R, $C);
        for (my $r = 0; $r < $R; $r++) {
            for (my $c = 0; $c < $C; $c++) {
                next if $painting[$r][$c] eq ".";
                if (
                        $painting[$r][$c] eq "#"
                        and 
                        $painting[$r][$c+1] eq "#"
                        and 
                        $painting[$r+1][$c] eq "#"
                        and 
                        $painting[$r+1][$c+1] eq "#"
                   ) {
                        $painting[$r][$c] = "/";
                        $painting[$r][$c+1] = "\\";
                        $painting[$r+1][$c] = "\\";
                        $painting[$r+1][$c+1] = "/";

                        $c++;
                }
                elsif ($painting[$r][$c] eq "#") {
                    print "Impossible\n";
                    next TESTCASE;
                }
            }
        }
        painting(\@painting, $R, $C);
    }
}

sub painting {
    my ($ref, $r, $c) = @_;
    for my $i (0 .. $r-1) {
        for my $j (0 .. $c-1) {
            print "$ref->[$i][$j]";
        }
        print "\n";
    }
}

main();

