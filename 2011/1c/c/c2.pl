#! /usr/bin/perl

use strict;
use List::Util qw(max first sum reduce);
use Data::Dumper qw(Dumper);

sub debug {
    local $" = "\n";
    print "@_";
}

sub gcd {
    my ($a, $b) = @_;
    my ($f, $s) = ($a, $b);
    my ($c);
    while (1) {
        $c = $a % $b;
        if ($c == 0) {
            return $b;
        }
        $a = $b;
        $b = $c;
    }
}

sub lcm {
    my ($a, $b) = @_;
    return $a * $b / gcd($a, $b);
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    my @data = ();
    for my $t (1 .. $T) {
        my ($N, $L, $H) = split /\s+/, shift @lines;
        my @freq = sort { $a <=> $b } split /\s+/, shift @lines;


        my $lcm = 1;
        my $state = "lower";
        my $return;
        foreach my $freq (@freq) {
            if ($state = "lower") {
                $lcm = lcm 1, $freq;
            }
            if ($lcm > $H) {
                $state = "higher";
            }
        }
        if ($lcm < $L) {
            my $i = 2;
            my $l;
            do {
                $l = $lcm * $i;
                if ($l > $H) {
                    $return = "Impossible";
                }
                elsif ($l >= $L) {
                    $return = $l;
                }
                $i++;
            }
        }
        $lcm = reduce { lcm $a, $b } grep { $_ < $H } @freq;
        print "Case #$t: [@freq] ";
        foreach my $big (grep { $_ >= $H } @freq) {
            if ($big % $lcm) {
                print "NO\n";
                last;
            }
        }
        if ($L <= $lcm and $lcm <= $H) {
            print "$lcm\n";
        } else {
            print "NO\n";
        }

##         print Dumper(\%data);
##     print Dumper \%stats;
    }
}

main();

