#! /usr/bin/perl

use strict;
use List::Util qw(max first sum reduce);
use Data::Dumper qw(Dumper);

sub debug {
    local $" = "\n";
    print "@_";
}

sub gcd {
    my ($a, $b) = @_;
    my ($f, $s) = ($a, $b);
    my ($c);
    while (1) {
        $c = $a % $b;
        if ($c == 0) {
            return $b;
        }
        $a = $b;
        $b = $c;
    }
}

sub lcm {
    my ($a, $b) = @_;
    return $a * $b / gcd($a, $b);
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    my @data = ();
TESTCASE:
    for my $t (1 .. $T) {
        my ($N, $L, $H) = split /\s+/, shift @lines;
        my @freq = split /\s+/, shift @lines;
        my $lcm = reduce { lcm $a, $b } grep { $_ < $H } @freq;
        print "Case #$t: ";
        my $i = 0;
        my $ans = $L;
        while ($ans <= $H and $i <= $#freq) {
            if ($freq[$i] % $ans == 0 or $ans % $freq[$i] == 0) {

            } else {
##                 print "Yes $freq[$i] and $ans\n";
##                 print "i=$i, f=$freq[$i], ans=$ans\n";
                $ans++;
            }
        }
        print "NO\n";

##         print Dumper(\%data);
##     print Dumper \%stats;
    }
}

main();

