#! /usr/bin/perl

use strict;
use List::Util qw(max first sum reduce);
use Data::Dumper qw(Dumper);

sub debug {
    local $" = "\n";
    print "@_";
}

sub gcd {
    my ($a, $b) = @_;
    my ($f, $s) = ($a, $b);
    my ($c);
    while (1) {
        $c = $a % $b;
        if ($c == 0) {
            return $b;
        }
        $a = $b;
        $b = $c;
    }
}

sub lcm {
    my ($a, $b) = @_;
    return $a * $b / gcd($a, $b);
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    my @data = ();
TESTCASE:
    for my $t (1 .. $T) {
        my ($N, $L, $H) = split /\s+/, shift @lines;
        my @freq = split /\s+/, shift @lines;
        my $lcm = reduce { lcm $a, $b } grep { $_ < $H } @freq;
        $lcm ||= 1;
        print "Case #$t: ";
##         print "Entering with lcm=$lcm [@freq] $H\n";
        foreach my $big (grep { $_ >= $H } @freq) {
##             print "big=$big, lcm=$lcm\n";
            if ($big % $lcm) {
                print "NO\n";
                next TESTCASE;
            }
        }
        if ($L <= $lcm and $lcm <= $H) {
            print "$lcm\n";
        } else {
            print "NO\n";
        }
        if ($lcm < $L) {
            my $l;
            my $i = 2;
            while (1) {
                $l = $lcm * $i;
                if ($l > $H) {
                    print "NO\n";
                    next TESTCASE;
                }
                if ($l >= $L) {
                    print "$l\n";
                    next TESTCASE;
                }
                $i++;
            }
        }

##         print Dumper(\%data);
##     print Dumper \%stats;
    }
}

main();

