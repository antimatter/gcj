#! /usr/bin/perl

use strict;
use List::Util qw(max first sum reduce);
use Data::Dumper qw(Dumper);

sub debug {
    local $" = "\n";
    print "@_";
}

sub gcd {
    my ($a, $b) = @_;
    my ($f, $s) = ($a, $b);
    my ($c);
    while (1) {
        $c = $a % $b;
        if ($c == 0) {
            return $b;
        }
        $a = $b;
        $b = $c;
    }
}

sub lcm {
    my ($a, $b) = @_;
    return $a * $b / gcd($a, $b);
}

sub main {
    my @lines = map { chomp; $_ } <>;
    my $T = shift @lines;
    my @data = ();
TESTCASE:
    for my $t (1 .. $T) {
        my ($N, $L, $H) = split /\s+/, shift @lines;
        my @freq = sort { $a <=> $b } split /\s+/, shift @lines;
##         my $lcm = reduce { lcm $a, $b } grep { $_ < $H } @freq;
        my $lcm = 1;
        my $i = 0;
        print "Case #$t: ";
        until ($freq[$i] > $H or $i > $#freq) {
##             print "\nCalc lcm $lcm, $freq[$i] $i ($#freq)\n";
            $lcm = lcm $lcm, $freq[$i];
            if ($lcm > $H) {
                print "NO\n";
                next TESTCASE;
            }
            if ($i == $#freq) {
                last;
            }
            $i++
        }
        if ($i == $#freq) {
            if ($L <= $lcm and $lcm <= $H) {
                print "$lcm\n";
                next TESTCASE;
            }
            elsif ($lcm < $L) {
                my $i = 2;
                $i++ while $lcm * $i < $L;
                if ($lcm * $i <= $H) {
                    print "$lcm\n";
                    next TESTCASE;
                } else {
                    print "NO2\n";
                    next TESTCASE;
                }
            }
        } else {
            print "For [@freq] - $i - $lcm\n";
            while ($i <= $#freq) {
                if ($lcm % $freq[$i] or $freq[$i] % $lcm) {
                    print "NO3\n";
                    next TESTCASE;
                }
            }
            print "$lcm\n";
        }
    }
}

main();

