#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func


@tail_call_optimized
def howmany(sarray, position, number, standing, additions):
    #print "howmany(%s, %d, %d, %d, %d)" % (sarray, position, number, standing, additions)
    if position == number:
        return additions

    #print "for %d - %d are standing and %d additions" % (position, standing, additions)
    if sarray[position] == 0:
        return howmany(sarray, position+1, number, standing, additions)
    elif standing+additions >= position:
        #print "For %d, %d more people stood up" % (position, sarray[position])
        return howmany(sarray, position+1, number, standing+sarray[position], additions)
    else:
        #print "For %d, added %d more people" % (position, position-(standing+additions))
        return howmany(sarray, position+1, number, standing+sarray[position], additions+(position-standing))


def main():
    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))
    testcase = 1
    while testcase <= T:
        Smax, s = (lines.pop(0)).rstrip().split(" ")
        s = [ int(x) for x in s ]
#        Smax = int(Smax)
        sum = 0
        sums = []
        for i in range(len(s)):
            sum += s[i]
            sums.append(sum)
        print "Case #%d:" % (testcase),
        print howmany(s, 0, len(s), 0, 0)
        testcase += 1

if __name__ == '__main__':
    main()
