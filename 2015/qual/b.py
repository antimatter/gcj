#! /usr/bin/python
import sys
import pprint
pp = pprint.PrettyPrinter(indent=4).pprint

class TailRecurseException:
  def __init__(self, args, kwargs):
    self.args = args
    self.kwargs = kwargs

def tail_call_optimized(g):
  """
  This function decorates a function with tail call
  optimization. It does this by throwing an exception
  if it is it's own grandparent, and catching such
  exceptions to fake the tail call optimization.
  
  This function fails if the decorated
  function recurses in a non-tail context.
  """
  def func(*args, **kwargs):
    f = sys._getframe()
    if f.f_back and f.f_back.f_back \
        and f.f_back.f_back.f_code == f.f_code:
      raise TailRecurseException(args, kwargs)
    else:
      while 1:
        try:
          return g(*args, **kwargs)
        except TailRecurseException, e:
          args = e.args
          kwargs = e.kwargs
  func.__doc__ = g.__doc__
  return func

def mult(x, y):
    values = {
    	"1": {
    		"1": "1",
    		"i": "i",
    		"j": "j",
    		"k": "k",
    	},
    	"i": {
    		"1": "i",
    		"i": "-1",
    		"j": "k",
    		"k": "-j",
    	},
    	"j": {
    		"1": "j",
    		"i": "-k",
    		"j": "-1",
    		"k": "i",
    	},
    	"k": {
    		"1": "k",
    		"i": "j",
    		"j": "-i",
    		"k": "-1",
    	},
    }
#    negatives = 0
#    print "mult(%s, %s) = " % (x, y),
    negative = ""
    if x.startswith("-"):
#negatives += 1
        x = x[1:]
        negative = "-"
#    if y.startswith("-"):
#        negatives += 1
#        y = y[1:]
#    negative = ""
#    if negatives % 2 == 1:
#        negative = "-"
    v = values[x][y]
    if v.startswith("-") and negative == "-":
        negative = ""
        v = v[1:]
    m = negative + v
#    print m
    return m


# @tail_call_optimized
def ispossible(chararr, lookingfor):
#print "Looking for %s in %s" % (lookingfor, chararr)
    if len(lookingfor) == 0:
        return "NO"
    if chararr == lookingfor:
        return "YES"
    if chararr[0] == "-" and len(chararr) < len(lookingfor) + 2:
        return "NO"
    elif chararr[0] != "-" and len(chararr) < len(lookingfor) + 1:
        return "NO"
    if chararr[0] == lookingfor[0]:
        if ispossible(chararr[1:], lookingfor[1:]) == "YES":
            return "YES"
    newarr = []
    if chararr[0] == "-":
        newarr.extend(mult(chararr[0]+chararr[1], chararr[2]))
        newarr.extend(chararr[3:])
    else:
        newarr.extend(mult(chararr[0], chararr[1]))
        newarr.extend(chararr[2:])
    return ispossible(newarr, lookingfor)

def main():
    f = open(sys.argv[1])
    lines = f.readlines()
    T = int(lines.pop(0))

#    for i in ("1", "i", "j", "k"):
#        for j in ("1", "i", "j", "k"):
#            mult(i, j)
#    mult("-j", "i")
#    sys.exit(0)
    testcase = 1
    while testcase <= T:
        L, X = [ int(x) for x in lines.pop(0).rstrip().split(" ") ]
        base = lines.pop(0).rstrip()
        string = "".join([ base for x in range(X) ])
        print "Case #%d: %s" % (testcase, ispossible(list(string), list("ijk")))
        testcase += 1

if __name__ == '__main__':
    main()
