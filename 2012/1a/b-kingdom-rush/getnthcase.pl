#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Data::Dump qw(dump);

my $required = $ARGV[1];

my @lines = map { chomp; $_ } `cat $ARGV[0]`;
shift @lines;


my $testcase = 1;
while (@lines) {
    my $N = shift @lines;
    my @data;
    push @data, shift @lines for 1 .. $N;
    if ($testcase == $required) {
        $" = "\n";
        print "$N\n@data\n";
        last;
    }
    $testcase++;
}

