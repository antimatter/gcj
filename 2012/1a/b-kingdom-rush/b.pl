#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Data::Dump qw(dump);

my @lines = map { chomp; $_ } <>;

my ($T) = split /\s/, shift @lines;

#print Dumper find_at_least_any([12, 8, 4, 2, 1], 0);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 13);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 1);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 12);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 2);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 8);
#print Dumper find_at_least_any([12, 8, 4, 3, 2], 4);
#print Dumper find_at_least_any([12, 8, 4, 3, 2, 1], 4);
#print Dumper find_first(13, 11, 10, 9, 6, 6, 6, 6, 3, 2, 2);
#exit;

foreach my $testcase (1 .. $T) {
    my $N = shift @lines;
    my @levelline;
    push @levelline, shift @lines for 1 .. $N;
    my %levels;
    my %lookup;
    for (my $i = 0; $i < @levelline; $i++) {
        my ($f, $s) = split /\s+/, $levelline[$i];
        $levels{1}{$i} = $f;
        $levels{2}{$i} = $s;
        push @{$lookup{$f}}, $i;
    }

    print Dumper \%lookup;
    foreach my $key (keys %lookup) {
        @{$lookup{$key}} = sort { $levels{2}{$b} <=> $levels{2}{$a} }
                           @{$lookup{$key}};
    }
    my @lookupsorted = sort { $b <=> $a } keys %lookup;
    my $points = 0;
    my @level2 = sort { $b->[0] <=> $a->[0] }
                 map { $levels{2}{$_} = (split /\s+/, $levelline[$_])[1];
                       [ $levels{2}{$_}, $_ ] }
                 0 .. $#levelline;
    @level2 = sort { $levels{2}{$b} <=> $levels{2}{$a} } keys %{$levels{2}};
    my @level1 = keys %{$levels{1}};
    @level1 = sort { $levels{2}{$b} <=> $levels{2}{$a} } keys %{$levels{1}};
    my $times = 0;

    my $t;
    my $start;
TURN:
    while (@level2) {
        $start = time();

        debug("Here with $points points: \n");
        debug(dump(\%levels) . "\n");
        my $index = 4;
        $start = time;
        $index = find_at_least_any(@level2, $points);
        $t = time - $start;
        my $i = defined $index ? $index : "undef";
        if (defined $index) {
            if (not defined $levels{1}{$index}) {
                $points += 1;
            }
            else {
                delete $levels{1}{$index};
                $points += 2;
            }
            $times++;
            debug("Played level 2 - $index. Now $points [$times]\n");
            splice @level2, $index, 1;
            delete $levels{2}{$index};
            next TURN;
        }
        $t = time - $start;
        $start = time;
        debug("Took $t to go through the L2s\n");

        $points = 4;
        @level1 = (9, 9, 8, 5, 5, 3, 2, 2, 1);
        my @l = ();
        $index = find_first(@level1, $points);
        if (defined $index) {
            @l = sort { $levels{2}{$b} <=> $levels{2}{$b} }
                 @level1[$index..$#level1];
        }
#@level1 = sort { $levels{2}{$b} <=> $levels{2}{$a} }
#                  grep { $points >= $levels{1}{$_} }
#                  keys %{$levels{1}};
        $t = time - $start;
        debug("Took $t to sort L1s\n");
        if (@l) {
            $points += 1;
            $times++;
            debug("Played level 1 - $l[0]. Now $points [$times]\n");
            delete $levels{1}{$l[0]};
            next TURN;
        }
        $t = time - $start;
        debug("Took $t to go through the L1s\n");
        last;
    }
    $times = "Too Bad" if @level2;
    print "Case #$testcase: $times\n";
}

sub find_first {
    my $points = pop;
    my @array = @_;
    my ($from, $to) = (0, scalar @array);
    return undef if $array[$#array] > $points;
    while ($to > $from) {
        my $index = $from + int(($to-$from)/2);
        if ($points < $array[$index]) {
            $from = $index + 1;
        }
        else {
            $to = $index;
        }
    }
    return $from;
}

sub find_at_least_any {
    my $start = time();
    my $t;
    my $points = pop;
    my @array = @_;
    #my ($array_ref, $points) = @_;
    my ($from, $to) = (0, scalar @array);

    while ($to > $from) {
        my $index = $from+int(($to-$from)/2);
        if ($array[$index] <= $points) {
            $t = time-$start;
            return $index;
        }
        else {
            $from = $index+1;
        }
    }
    return undef;



    my $array_ref;
    print "was here with $points points. from = $from, to = $to\n";
    while ($to > $from) {
        my $index = int(($to-$from)/2);
        if ($array_ref->[$index] <= $points) {
            print "Would have returned $index\n";
            exit;
            return $index;
        }
        else {
            $from = $index;
        }
    }
    print "Returned undef :(\n";
    exit;
    return undef;
}

sub debug {
    return;
    print "@_";
}

