#! /usr/bin/perl -w

use strict;
use Data::Dumper qw(Dumper);
use Data::Dump qw(dump);

my @lines = map { chomp; $_ } <>;

my ($T) = split /\s/, shift @lines;

#print Dumper find_at_least_any([12, 8, 4, 2, 1], 0);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 13);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 1);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 12);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 2);
#print Dumper find_at_least_any([12, 8, 4, 2, 1], 8);
#print Dumper find_at_least_any([12, 8, 4, 3, 2], 4);
#print Dumper find_at_least_any([12, 8, 4, 3, 2, 1], 4);
#print Dumper find_first(13, 11, 10, 9, 6, 6, 6, 6, 3, 2, 2);
#exit;

foreach my $testcase (1 .. $T) {
    my $N = shift @lines;
    my @levelline;
    push @levelline, shift @lines for 1 .. $N;
    my %levels;
    my %pointsrequired1;
    my %pointsrequired2;
    for (my $i = 0; $i < @levelline; $i++) {
        my ($f, $s) = split /\s+/, $levelline[$i];
        $levels{1}{$i} = $f;
        $levels{2}{$i} = $s;
        push @{$pointsrequired1{$f}}, $i;
        push @{$pointsrequired2{$s}}, $i;
    }

    foreach my $key (keys %pointsrequired1) {
        @{$pointsrequired1{$key}} = sort { $levels{2}{$b} <=> $levels{2}{$a} }
                           @{$pointsrequired1{$key}};
    }
    my @pointsrequired1sorted = sort { $b <=> $a } keys %pointsrequired1;
    my @pointsrequired2sorted = sort { $b <=> $a } keys %pointsrequired2;
    dump @pointsrequired1sorted;
    dump @pointsrequired2sorted;
    dump \%levels;
    dump \%pointsrequired1;
    dump \%pointsrequired2;
    my $points = 0;
    my $times = 0;

TURN:
    while (@pointsrequired2sorted) {
        debug("Here with $points points: \n");
        my $p2index = find_at_least_any(@pointsrequired2sorted, $points);
        print "Trying Level 2 (points: $points)\n";
        dump \@pointsrequired2sorted;
        if (defined $p2index) {
            my $level = $pointsrequired2{$pointsrequired2sorted[$p2index]}->[0];
            if (not defined $levels{1}{$level}) {
                $points += 1;
            }
            else {
                my $level1points = $levels{1}{$level};
                delete_from_points_required(\%pointsrequired1,
                                            $levels{1},
                                            $level);
                delete $levels{1}{$level};
                unless (@{$pointsrequired1{$level1points}}) {
                    delete $pointsrequired1{$level1points};
                }
                $points += 2;
            }
            $times++;
            debug("Played level 2 - $level. Now $points [$times]\n");
            delete $levels{2}{$level};
            splice @{$pointsrequired2{$level}}, 0, 1;
            unless (@{$pointsrequired2{$level}}) {
                delete $pointsrequired2{$level};
                @pointsrequired2sorted = grep { $_ != $level }
                                         @pointsrequired2sorted;
            }
            next TURN;
        }

        my $i = undef;
        my $level1points;
        my $p1index = find_first(@pointsrequired1sorted, $points);
        print "Trying Level 2\n";
        if (defined $p1index) {
            dump \%pointsrequired1;
            dump [ @pointsrequired1sorted[$p1index..$#pointsrequired1sorted] ];
            $level1points = (sort { $levels{2}{$pointsrequired1{$b}[0]}
                          <=>
                        $levels{2}{$pointsrequired1{$a}[0]} }
                  @pointsrequired1sorted[$p1index..$#pointsrequired1sorted])[0];

            my $level1 = $pointsrequired1{$level1points}[0];

            $points += 1;
            $times++;
            debug("Played level 1 - $level1. Now $points [$times]\n");
            delete $levels{1}{$level1};
            splice @{$pointsrequired1{$level1points}}, 0, 1;
            unless (@{$pointsrequired1{$level1points}}) {
                delete $pointsrequired1{$level1points};
                @pointsrequired1sorted = grep { $_ != $level1points }
                                         @pointsrequired1sorted;
            }
            next TURN;
        }
        die;
        last;
    }
    $times = "Too Bad" if @pointsrequired2sorted;
    print "Case #$testcase: $times\n";
}

sub delete_from_points_required {
    my ($pointsreq_ref, $levels_ref, $level) = @_;
    my $points = $levels_ref->{$level};
    $pointsreq_ref->{$points} = [ grep { $_ != $level }
                                  @{$pointsreq_ref->{$points}} ];
}

sub find_first {
    my $points = pop;
    my @array = @_;
    my ($from, $to) = (0, scalar @array);
    return undef if $array[$#array] > $points;
    while ($to > $from) {
        my $index = $from + int(($to-$from)/2);
        if ($points < $array[$index]) {
            $from = $index + 1;
        }
        else {
            $to = $index;
        }
    }
    return $from;
}

sub find_at_least_any {
    my $start = time();
    my $t;
    my $points = pop;
    my @array = @_;
    #my ($array_ref, $points) = @_;
    my ($from, $to) = (0, scalar @array);

    while ($to > $from) {
        my $index = $from+int(($to-$from)/2);
        if ($array[$index] <= $points) {
            $t = time-$start;
            return $index;
        }
        else {
            $from = $index+1;
        }
    }
    return undef;



    my $array_ref;
    while ($to > $from) {
        my $index = int(($to-$from)/2);
        if ($array_ref->[$index] <= $points) {
            print "Would have returned $index\n";
            exit;
            return $index;
        }
        else {
            $from = $index;
        }
    }
    print "Returned undef :(\n";
    exit;
    return undef;
}

sub debug {
    return;
    print "@_";
}

