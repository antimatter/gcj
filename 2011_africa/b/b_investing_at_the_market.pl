#! /usr/bin/perl

use strict;
use warnings;
use File::Basename qw(basename);
use Data::Dumper qw(Dumper);

my $ME = basename $0;

scalar @ARGV != 1 and die "Usage: $ME <inputfile>\n";

## open FH, "<", $ARGV[0] or die "$ME: Unable to open file `$ARGV[0]'\n";
open(FH, "<", shift) or die "$ME: Unable to open file `$ARGV[0]'\n";
my @lines = map { chomp; $_ } <FH>;
close FH;

my $N = shift @lines;
use List::Util qw(sum max);

for (my $i = 1; $i <= $N; $i++) {
    my $M = int(shift @lines);
    my @L;
    my $month = 1;
    my %price = map { $month++ => $_ } split /\s+/, shift @lines;
    my %units = ();
    my %profits = ();
    for (my $month = 1; $month <= 12; $month++) {
        if ($price{$month} <= $M) {
            $units{$month} = int $M/$price{$month};
        }
        for (my $buymonth = 1; $buymonth < $month; $buymonth++) {
            next if not defined $units{$buymonth};
            next unless $price{$month} > $price{$buymonth};
            $profits{"$buymonth $month"} = $units{$buymonth} * ($price{$month} - $price{$buymonth});
        }
    }

    my $maxprofit = 0;
    my $maxprofit_buymonth = 0;
    my $maxprofit_months = "";
    foreach my $months (keys %profits) {
        my ($buy, $sell) = split / /, $months;
        if ($profits{$months} > $maxprofit
            or ($profits{$months} == $maxprofit
                and $price{$buy} < $price{$maxprofit_buymonth})) {
            $maxprofit = $profits{$months};
            $maxprofit_buymonth = $buy;
            $maxprofit_months = $months;
        }
    }
    print "Case #$i: ";
    if (%profits) {
        print "$maxprofit_months $profits{$maxprofit_months}\n";
    } else {
        print "IMPOSSIBLE\n";
    }
##     if ($i == 10) {
##         print Dumper \%price;
##         print Dumper \%profits;
##     }

}

