#! /usr/bin/perl

use strict;
use warnings;
use File::Basename qw(basename);
use Data::Dumper qw(Dumper);

my $ME = basename $0;

scalar @ARGV != 1 and die "Usage: $ME <inputfile>\n";

## open FH, "<", $ARGV[0] or die "$ME: Unable to open file `$ARGV[0]'\n";
open(FH, "<", shift) or die "$ME: Unable to open file `$ARGV[0]'\n";
my @lines = map { chomp; $_ } <FH>;
close FH;

my $N = shift @lines;
use List::Util qw(sum max);

sub biggest {
    my ($array_ref, $l_beg, $w_beg, $l_end, $w_end) = @_;
    print "Here for @{$array_ref}, $l_beg, $w_beg, $l_end, $w_end\n";
    if ($l_beg == $l_end or $w_beg == $w_end) {
        return 0;
    }
    for (my $w = $w_beg; $w < $w_end; $w++) {
        for (my $l = $l_beg; $l < $l_end; $l++) {
            print "$array_ref->[$w][$l] ";
            if ($array_ref->[$w][$l] eq '0') {
                return max(
##                             biggest($array_ref, $l_beg, $w_beg, $l, $w),
##                             biggest($array_ref, $l, $w_beg, $l_end, $w),
##                             biggest($array_ref, $l_beg, $w, $l, $w_end),
##                             biggest($array_ref, $l, $w, $l_end, $w_end),
                            biggest($array_ref, $l_beg, $w_beg, $l_end, $w),
                            biggest($array_ref, $l_beg, $w, $l, $w_end),
                            biggest($array_ref, $l_beg, $w_beg, $l, $w_end),
                            biggest($array_ref, $l, $w_beg, $l_end, $w_end),
                        );
            }
        }
        printf "Returning: %d\n", $l_end * $w_end;
        return $l_end * $w_end;
        print "\n";
    }
}



for (my $i = 1; $i <= $N; $i++) {
    my ($L, $W) = split / /, shift @lines;
    my @data = ();
    for ($i = 0; $i < $W; $i++) {
        my $row = shift @lines;
        $row =~ tr/TRWGS/00011/;
        push @data, [split //, $row];
    }
    print "Case #$i: $L $W\n";
    print Dumper \@data;
    print "\n";
biggest(\@data, 0, 0, $L, $W);
##     local $, = ",";
##     print @B[0 .. $segs-1];
##     print "\n";
##     print sum(@B[0 .. $segs-1]);
##     print "\n";
##     print sum(@R[0 .. $segs-1]);
##     print "\n";
##     print "segs = $segs\n";
}

