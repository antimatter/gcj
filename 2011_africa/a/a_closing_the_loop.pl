#! /usr/bin/perl

use strict;
use warnings;
use File::Basename qw(basename);
use Data::Dumper qw(Dumper);

my $ME = basename $0;

scalar @ARGV != 1 and die "Usage: $ME <inputfile>\n";

## open FH, "<", $ARGV[0] or die "$ME: Unable to open file `$ARGV[0]'\n";
open(FH, "<", shift) or die "$ME: Unable to open file `$ARGV[0]'\n";
my @lines = map { chomp; $_ } <FH>;
close FH;

my $N = shift @lines;
use List::Util qw(sum);

for (my $i = 1; $i <= $N; $i++) {
    my $S = shift @lines;
    my @L = split /\s+/, shift @lines;
    my @B = reverse sort { $a <=> $b } map { s/B//; int($_) } grep { /B/ } @L;
    my @R = reverse sort { $a <=> $b } map { s/R//; int($_) } grep { /R/ } @L;
    my $segs = scalar @B > scalar @R ? scalar @R : scalar @B;
    no warnings;

    my $maxlength = 0;
    if ($segs) {
        $maxlength = sum(@B[0 .. $segs-1], @R[0 .. $segs-1]) - (2*$segs);
    }
    print "Case #$i: $maxlength\n";
##     local $, = ",";
##     print @B[0 .. $segs-1];
##     print "\n";
##     print sum(@B[0 .. $segs-1]);
##     print "\n";
##     print sum(@R[0 .. $segs-1]);
##     print "\n";
##     print "segs = $segs\n";
}

